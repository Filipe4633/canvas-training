/**
 * Sistemas de gastos familiares
 * 
 * Cria um objeto que posuira 2 propriedade, ambas do tpo array:
 *  * receitas:[]
 *  * despesas:[]
 * 
 * agora, crie uma função que irá calcular p total de receitas
 * e despesas e ira mostra uma messagem se a familia esta com
 * slado positivo ou negativo, seguido do valor do saldo. * 
 * 
 */

 let family = {
    receitas : [200, 300, 2000] ,

    despesas : [100, 500, 1000, 45, 4000 ]

 }
function soma(array){
    let total=0

    for(let valor of array){
        total +=  valor
    }

    return total
}
 function calcularBalaco(){
     const calculaReceitas = soma(family.receitas)
     const calcularDespesas = soma(family.despesas)
     const total = calculaReceitas - calcularDespesas
     const estaOk = total >= 0
     let  balanco
     
     if(estaOk){
       balanco= "Positivo"
     }
     else{
         balanco = "Negativo"
        }

     console.log('Seu está '+balanco)
     console.log("com saldo de "+total)

 }
calcularBalaco()